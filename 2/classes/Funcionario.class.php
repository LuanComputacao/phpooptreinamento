<?php
class Funcionario
{
	private 	$Codigo;
	public 		$Nome;
	private		$Nascimento;
	protected	$Salario;
	
	/*M�todo SetSalario
	 * atribui o par�metro $Salario � propriedade $Salario
	 */
	
	function SetNome($Nome)
	{
		if (is_string($Nome)) {
			$this->Nome=$Nome;
		}
	}
	
	function SetSalario($Salario)
	{
		//verifica se � num�rico e positivo
		if (is_numeric($Salario) and ($Salario>0)) {
			$this->Salario = $Salario;
		}
	}
	
	function GetNome()
	{
		return $this->Nome;		
	}
	
	/*M�todo GetSalario
	 * retorna o valor da propriedade $Salario
	 */
	function GetSalario(){
		return $this->Salario;
	}
}
?>