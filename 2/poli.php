<?php
	#carrega as  classes
	include_once 'classes/Pessoa.class.php';
	include_once 'classes/Conta.class.php';
	include_once 'classes/ContaPoupanca.class.php';
	include_once 'classes/ContaCorrente.class.php';
	
	#Cria��o do objeto $carlos
	$carlos = new Pessoa(10, "Carlos da Silva", 1.85, 25, "10/04/02", "Ensino M�dio", 650.00);
	
	echo "Manipulando o objeto {$carlos->Nome}";
	#Cria��o do objeto $conta_carlos
	$contas[1] = new ContaCorrente(6677, "CC.1234.56", "10/07/02", $carlos, 9876, 500, 200.00);
	$contas[2] = new ContaCorrente(6678, "PP.1234.56", "10/07/02", $carlos, 9876, 500, '10/07');
	
	//percorremos as contas
	foreach ($contas as $key=> $conta)
	{
		echo "</br> Manipulando a conta $key de: {$conta->Titular->Nome}: </br>";
		echo "O salario atual da contqa $key � R/S {$conta->ObterSaldo()}</br> ";
		$conta->Depositar(200);
		echo "O salario atual da contqa $key � R/S {$conta->ObterSaldo()}</br> ";
		$conta->Retirar(100);
		echo "O salario atual da contqa $key � R/S {$conta->ObterSaldo()}</br> ";
	}
	
?>