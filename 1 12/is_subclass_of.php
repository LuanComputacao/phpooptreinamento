<?php
	class Funcionario
	{
		var $codigo;
		var $nome;
	}	

	class Estagiario extends Funcionario
	{
		var $salario;
		var $nome;
	}

	$jose = new Estagiario;

	if (is_subclass_of($jose, 'Funcionario')) {
		echo "Classe do objeto Jose é derivada de Funcionario";
	}

	echo "</br>";

	if (is_subclass_of('Estagiario', 'Funcionario')) {
		echo "Classe Estagiario é derivada de Funcionario";
	}
?>